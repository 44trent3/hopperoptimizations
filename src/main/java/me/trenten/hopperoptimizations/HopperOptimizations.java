package me.trenten.hopperoptimizations;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.plugin.Plugin;

@Plugin(
        id = "hopperoptimizations",
        name = "HopperOptimizations",
        description = "Port of https://github.com/2No2Name/hopperOptimizations",
        authors = {"2No2Name", "44trent3"})

public class HopperOptimizations {
    @Inject
    Logger logger;

    @Listener
    public void gameStartedEvent(GameStartedServerEvent event) {
        logger.info("hopperOptimizations plugin loaded...");
    }
}
